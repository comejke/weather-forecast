package com.rosberry.trial.weatherforecast.data.manager

interface LoginManagerCallback {
    fun onLogin()
    fun onLoginError(error: Int)
}