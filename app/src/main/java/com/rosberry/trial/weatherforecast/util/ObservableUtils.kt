package com.rosberry.trial.weatherforecast.util

import androidx.databinding.Observable

fun <T : Observable> T.addOnPropertyChangedCallback(callback: () -> Unit) =
    addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            callback()
        }
    })