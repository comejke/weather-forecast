package com.rosberry.trial.weatherforecast.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.rosberry.trial.weatherforecast.R
import com.rosberry.trial.weatherforecast.databinding.FragmentLoginBinding

class LoginFragment : Fragment() {

    private lateinit var viewModel: LoginViewModel
    private lateinit var binding: FragmentLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setViewModelObservers()

        binding = DataBindingUtil.inflate<FragmentLoginBinding>(
            inflater, R.layout.fragment_login, container, false
        ).apply {
            viewModel = this@LoginFragment.viewModel
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.tryCachedLogIn()
    }

    private fun setViewModelObservers() {
        viewModel.apply {
            onClickRegister.observe(viewLifecycleOwner, Observer { navigateRegister() })
            onLogin.observe(viewLifecycleOwner, Observer { navigateMainScreen() })
            onUsernameError.observe(viewLifecycleOwner, Observer { setUsernameError(it) })
            onPasswordError.observe(viewLifecycleOwner, Observer { setPasswordError(it) })
        }
    }

    private fun navigateRegister() {
        findNavController().navigate(R.id.action_login_to_register)
    }

    private fun navigateMainScreen() {
        // TODO navigate to main screen
    }

    private fun setUsernameError(error: Int) {
        binding.textUsernameError.text = when (error) {
            ERROR_NO_USERNAME -> resources.getString(R.string.login_err_no_username)
            ERROR_NO_USER -> resources.getString(R.string.login_err_no_user)
            else -> return
        }
    }

    private fun setPasswordError(error: Int) {
        binding.textPasswordError.text = when (error) {
            ERROR_NO_PASSWORD -> resources.getString(R.string.login_err_no_password)
            ERROR_WRONG_PASSWORD -> resources.getString(R.string.login_err_password)
            else -> return
        }
    }
}