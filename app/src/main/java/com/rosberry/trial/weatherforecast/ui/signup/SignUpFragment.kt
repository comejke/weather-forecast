package com.rosberry.trial.weatherforecast.ui.signup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.rosberry.trial.weatherforecast.R
import com.rosberry.trial.weatherforecast.databinding.FragmentSignupBinding

class SignUpFragment : Fragment() {

    private lateinit var viewModel: SignUpViewModel
    private lateinit var binding: FragmentSignupBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SignUpViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setViewModelObservers()

        binding = DataBindingUtil.inflate<FragmentSignupBinding>(
            inflater, R.layout.fragment_signup, container, false
        ).apply {
            viewModel = this@SignUpFragment.viewModel
        }

        return binding.root
    }

    private fun setViewModelObservers() {
        viewModel.apply {
            onClickBack.observe(viewLifecycleOwner, Observer { navigateBack() })
            onSignUp.observe(viewLifecycleOwner, Observer { navigateMainScreen() })
            onUsernameError.observe(viewLifecycleOwner, Observer { setUsernameError(it) })
        }
    }

    private fun navigateBack() {
        findNavController().popBackStack()
    }

    private fun navigateMainScreen() {
        // TODO navigate to main fragment
    }

    private fun setUsernameError(error: Int) {
        binding.textUsernameError.text = when (error) {
            ERROR_NO_USERNAME -> resources.getString(R.string.register_err_no_username)
            ERROR_USER_EXIST -> resources.getString(R.string.register_err_user_exist)
            else -> return
        }
    }
}
