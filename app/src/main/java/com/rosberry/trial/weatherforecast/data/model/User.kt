package com.rosberry.trial.weatherforecast.data.model

class User(
    val id: String?,
    val name: String?,
    val passwordHash: String
)