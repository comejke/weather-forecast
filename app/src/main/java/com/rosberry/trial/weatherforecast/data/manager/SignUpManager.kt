package com.rosberry.trial.weatherforecast.data.manager

import com.rosberry.trial.weatherforecast.data.local.PrefsHelper
import com.rosberry.trial.weatherforecast.data.model.User
import com.rosberry.trial.weatherforecast.util.md5
import java.util.*

class SignUpManager {

    private val prefsHelper: PrefsHelper by lazy {
        PrefsHelper.getInstance()
    }

    fun isUserExist(username: String): Boolean {
        return prefsHelper.containsUser(username)
    }

    fun registerUser(username: String, name: String, password: String) {
        prefsHelper.saveUser(
            username,
            User(UUID.randomUUID().toString(), name, password.md5())
        )
        prefsHelper.currentUser = username
    }

}