package com.rosberry.trial.weatherforecast

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.rosberry.trial.weatherforecast.data.local.PrefsHelper

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        Thread.sleep(500)
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        PrefsHelper.init(applicationContext)
    }

}