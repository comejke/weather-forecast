package com.rosberry.trial.weatherforecast.data.local

import android.content.Context
import androidx.core.content.edit
import com.google.gson.Gson
import com.rosberry.trial.weatherforecast.data.model.User
import com.rosberry.trial.weatherforecast.util.md5

private const val WF_PREFS = "WF_PREFS"

private const val KEY_CURRENT_USER = "CURRENT_USER"
private const val KEY_USER_PREFIX = "USER_"

class PrefsHelper private constructor(context: Context) {

    companion object {

        private lateinit var instance: PrefsHelper
        fun getInstance(): PrefsHelper {
            if (::instance.isInitialized) {
                return instance
            }

            throw UninitializedPropertyAccessException(
                "Call init(Context) before using this method."
            )
        }

        fun init(context: Context) {
            instance = PrefsHelper(context)
        }
    }

    private val prefs by lazy {
        context.getSharedPreferences(WF_PREFS, Context.MODE_PRIVATE)
    }

    var currentUser: String?
        get() = prefs.getString(KEY_CURRENT_USER, null)
        set(username) = prefs.edit { putString(KEY_CURRENT_USER, username) }

    fun containsUser(username: String): Boolean {
        return prefs.contains("$KEY_USER_PREFIX${username.md5()}")
    }

    fun getCachedUser(): User? {
        return currentUser?.let { getUser(it) }
    }

    fun getUser(username: String): User? {
        return Gson().fromJson<User>(
            prefs.getString("$KEY_USER_PREFIX${username.md5()}", null),
            User::class.java
        )
    }

    fun saveUser(username: String, user: User) {
        prefs.edit { putString("$KEY_USER_PREFIX${username.md5()}", Gson().toJson(user)) }
    }

    fun clearUser() {
        prefs.edit {
            remove(KEY_CURRENT_USER)
        }
    }
}