package com.rosberry.trial.weatherforecast.ui.login

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.rosberry.trial.weatherforecast.data.manager.LoginManager
import com.rosberry.trial.weatherforecast.data.manager.LoginManagerCallback
import com.rosberry.trial.weatherforecast.util.SingleLiveEvent
import com.rosberry.trial.weatherforecast.util.addOnPropertyChangedCallback

const val ERROR_NO_USERNAME = 0
const val ERROR_NO_USER = 1
const val ERROR_NO_PASSWORD = 2
const val ERROR_WRONG_PASSWORD = 3

class LoginViewModel : ViewModel(), LoginManagerCallback {

    val username = ObservableField<String>()
    val password = ObservableField<String>()
    val showUsernameError = ObservableBoolean()
    val showPasswordError = ObservableBoolean()

    val onClickRegister: LiveData<Any>
        get() = navigateRegisterEvent
    val onLogin: LiveData<Any>
        get() = loginEvent
    val onUsernameError: LiveData<Int>
        get() = usernameErrorEvent
    val onPasswordError: LiveData<Int>
        get() = passwordErrorEvent

    private val navigateRegisterEvent = SingleLiveEvent<Any>()
    private val loginEvent = SingleLiveEvent<Any>()
    private val usernameErrorEvent = SingleLiveEvent<Int>()
    private val passwordErrorEvent = SingleLiveEvent<Int>()
    private val loginManager = LoginManager(this)

    init {
        username.addOnPropertyChangedCallback { showUsernameError.set(false) }
        password.addOnPropertyChangedCallback { showPasswordError.set(false) }
    }

    fun onClickRegister() {
        navigateRegisterEvent.call()
    }

    fun onClickLogin() {
        val username = username.get()?.trim()
        val password = password.get()

        if (isInputValid(username, password)) {
            loginManager.logIn(username!!, password!!)
        }
    }

    fun tryCachedLogIn() {
        loginManager.cachedLogIn()
    }

    private fun isInputValid(username: String?, password: String?): Boolean {
        return isUsernameValid(username) && isPasswordValid(password)
    }

    private fun isUsernameValid(username: String?): Boolean {
        return when {
            username.isNullOrEmpty() -> {
                setUsernameError(ERROR_NO_USERNAME)
                false
            }
            else -> true
        }
    }

    private fun setUsernameError(error: Int) {
        usernameErrorEvent.value = error
        showUsernameError.set(true)
    }

    private fun isPasswordValid(password: String?): Boolean {
        return when {
            password.isNullOrEmpty() -> {
                setPasswordError(ERROR_NO_PASSWORD)
                false
            }
            else -> true
        }
    }

    private fun setPasswordError(error: Int) {
        passwordErrorEvent.value = error
        showPasswordError.set(true)
    }

    override fun onLogin() {
        loginEvent.call()
    }

    override fun onLoginError(error: Int) {
        when (error) {
            ERROR_NO_USER -> setUsernameError(error)
            ERROR_WRONG_PASSWORD -> setPasswordError(error)
        }
    }
}
