package com.rosberry.trial.weatherforecast.data.manager

import com.rosberry.trial.weatherforecast.data.local.PrefsHelper
import com.rosberry.trial.weatherforecast.ui.login.ERROR_NO_USER
import com.rosberry.trial.weatherforecast.ui.login.ERROR_WRONG_PASSWORD
import com.rosberry.trial.weatherforecast.util.md5

class LoginManager(private val callback: LoginManagerCallback) {

    private val prefsHelper: PrefsHelper by lazy {
        PrefsHelper.getInstance()
    }

    fun cachedLogIn() {
        if (prefsHelper.getCachedUser() != null) {
            callback.onLogin()
        }
    }

    fun logIn(username: String, password: String) {
        val user = prefsHelper.getUser(username)
        when {
            user == null -> callback.onLoginError(ERROR_NO_USER)
            user.passwordHash != password.md5() -> callback.onLoginError(ERROR_WRONG_PASSWORD)
            else -> {
                prefsHelper.currentUser = username
                callback.onLogin()
            }
        }
    }

}