package com.rosberry.trial.weatherforecast.ui.signup

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.rosberry.trial.weatherforecast.data.manager.SignUpManager
import com.rosberry.trial.weatherforecast.util.SingleLiveEvent
import com.rosberry.trial.weatherforecast.util.addOnPropertyChangedCallback

const val ERROR_NO_USERNAME = 0
const val ERROR_USER_EXIST = 1

class SignUpViewModel : ViewModel() {

    val username = ObservableField<String>()
    val name = ObservableField<String>()
    val password = ObservableField<String>()
    val confirmation = ObservableField<String>()
    val showUsernameError = ObservableBoolean()
    val showNameError = ObservableBoolean()
    val showPasswordError = ObservableBoolean()
    val showConfirmationError = ObservableBoolean()

    val onClickBack: LiveData<Any>
        get() = clickBackEvent
    val onSignUp: LiveData<Any>
        get() = singUpEvent
    val onUsernameError: LiveData<Int>
        get() = usernameErrorEvent

    private val clickBackEvent = SingleLiveEvent<Any>()
    private val singUpEvent = SingleLiveEvent<Any>()
    private val usernameErrorEvent = SingleLiveEvent<Int>()
    private val signUpManager = SignUpManager()

    init {
        username.addOnPropertyChangedCallback { showUsernameError.set(false) }
        name.addOnPropertyChangedCallback { showNameError.set(false) }
        password.addOnPropertyChangedCallback { showPasswordError.set(false) }
        confirmation.addOnPropertyChangedCallback { showConfirmationError.set(false) }
    }

    fun onClickBack() {
        clickBackEvent.call()
    }

    fun onClickRegister() {
        val username = username.get()?.trim()
        val name = name.get()?.trim()
        val password = password.get()
        val confirmation = confirmation.get()

        if (isInputValid(username, name, password, confirmation)) {
            signUpManager.registerUser(username!!, name!!, password!!)
            singUpEvent.call()
        }
    }

    private fun isInputValid(
        username: String?, name: String?, password: String?, confirmation: String?
    ): Boolean {
        return isUsernameValid(username)
                && isNameValid(name)
                && isPasswordValid(password)
                && isConfirmationValid(password, confirmation)
    }

    private fun isUsernameValid(username: String?): Boolean {
        return when {
            username.isNullOrEmpty() -> {
                usernameErrorEvent.value = ERROR_NO_USERNAME
                showUsernameError.set(true)
                false
            }
            isUserExist(username) -> {
                usernameErrorEvent.value = ERROR_USER_EXIST
                showUsernameError.set(true)
                false
            }
            else -> true
        }
    }

    private fun isUserExist(username: String): Boolean {
        return signUpManager.isUserExist(username)
    }

    private fun isNameValid(name: String?): Boolean {
        return when {
            name.isNullOrEmpty() -> {
                showNameError.set(true)
                false
            }
            else -> true
        }
    }

    private fun isPasswordValid(password: String?): Boolean {
        return when {
            password.isNullOrEmpty() -> {
                showPasswordError.set(true)
                false
            }
            else -> true
        }
    }

    private fun isConfirmationValid(password: String?, confirmation: String?): Boolean {
        return when {
            confirmation.isNullOrEmpty() || confirmation != password -> {
                showConfirmationError.set(true)
                false
            }
            else -> true
        }
    }
}